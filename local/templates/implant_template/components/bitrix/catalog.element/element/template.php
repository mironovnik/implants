

<div class="product_card">
    <h1><?=$arResult['NAME']?></h1>
    <div class="product_price">
        Цена товара: <?=$arResult["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"];?>
    </div>
    <div class="product_image">
        <img src="<?=$arResult['PREVIEW_PICTURE']['SRC'];?>" width="100px" height="100px"  alt="">
    </div>
    <div >
        <button data-id="<?=$arResult['ID']?>" class="btn product-to-cart">Добавить в корзину</button>
    </div>
</div>