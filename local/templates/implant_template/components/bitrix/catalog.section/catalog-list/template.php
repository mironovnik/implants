
<? $APPLICATION->IncludeComponent(
    "bitrix:menu",
    "catalog_sections",
    array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "catalog_sections",
        "DELAY" => "N",
        "MAX_LEVEL" => "3",
        "MENU_CACHE_GET_VARS" => array(),
        "MENU_CACHE_TIME" => "36000000",
        "MENU_CACHE_TYPE" => "Y",
        "MENU_CACHE_USE_GROUPS" => "N",
        "ROOT_MENU_TYPE" => "catalog-section",
        "USE_EXT" => "Y",
        "COMPONENT_TEMPLATE" => "catalog_sections"
    ),
    false
); ?>
<div id="content">
    <div id="catalog">
        <div id="catalog-list">
            <?foreach($arResult["ITEMS"] as $arItem ):?>
            <?

            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="catalog-item">
                <img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" width="100%" alt="">
                <div class="catalog-item-price"><?=$arItem["DISPLAY_PROPERTIES"]["PRICE"]["VALUE"]?></div>
                <div class="catalog-item-hover">
                    <span><?=$arItem["PREVIEW_TEXT"]?></span>
                    <a href="<?=$arItem["DETAIL_PAGE_URL"]?>">подробнее</a>
                </div>
            </div>
            <?endforeach;?>
        </div>
    </div>
</div>




