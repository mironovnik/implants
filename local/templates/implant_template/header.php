<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title><? $APPLICATION->ShowTitle() ?></title>
    <? $APPLICATION->ShowHead() ?>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&amp;subset=cyrillic"
          rel="stylesheet">
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH . '/css/style.css' ?>">
    <script src="<?= SITE_TEMPLATE_PATH . '/js/jquery-3.1.1.min.js' ?>"></script>
    <script src="<?= SITE_TEMPLATE_PATH . '/js/functions.js' ?>"></script>
    <script src="<?= SITE_TEMPLATE_PATH . '/js/custom.js' ?>"></script>
    <link rel="icon" href="<?= SITE_TEMPLATE_PATH . '/img/fav.png' ?>" type="image/x-icon">
    <link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH . '/img/fav.png' ?>" type="image/x-icon">
</head>
<body>
<? $APPLICATION->ShowPanel() ?>
<div id="content-size"></div>
<div id="all-content">
    <header>
        <a href="/">LOGO</a>
        <div id="header-right">
            <a href="/basket/">Корзина</a>
            <a href="/auth/">Войти</a>
        </div>
    </header>
